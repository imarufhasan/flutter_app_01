import 'package:flutter/material.dart';
import 'package:flutter_app_01/SecondScreen.dart';
import 'package:flutter_app_01/screence/note_details.dart';


void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Simple Interest Calculator App',
    home: SIForm(),
    theme: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.indigo,
      accentColor: Colors.indigoAccent,
    ),
  ));
}

class SIForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SIFromState();
  }
}

class _SIFromState extends State<SIForm> {
  var _FromKey = GlobalKey<FormState>();
  var _currencies = ['Taka', 'Dollars', 'Paunds', 'Rupees'];
  var _currenciesSelected = 'Taka';
  final _minimumPadding = 5.0;
  TextEditingController principal_textController = TextEditingController();
  TextEditingController interest_textController = TextEditingController();
  TextEditingController tirm_textController = TextEditingController();
  var displayResult = '';

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    return Scaffold(
        //resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Simple Tnterest Calculator'),
        ),
        body: Form(
          key: _FromKey,
          child: Padding(
            padding: EdgeInsets.all(_minimumPadding * 2),
            child: ListView(
              children: <Widget>[
                //setImage

                //getImageAsset(),

                //PrincipalTextFeild
                Padding(
                  padding: EdgeInsets.only(
                      top: _minimumPadding*20, bottom: _minimumPadding),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    style: textStyle,
                    controller: principal_textController,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please enter principle amount';
                      }
                    },
                    decoration: InputDecoration(
                        labelText: 'Principal',
                        hintText: 'Enter Principal e.g 12000',
                        labelStyle: textStyle,
                        errorStyle: TextStyle(
                            color: Colors.yellowAccent, fontSize: 15.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
                //InterestTextFeild
                Padding(
                  padding: EdgeInsets.only(
                      top: _minimumPadding, bottom: _minimumPadding),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    style: textStyle,
                    controller: interest_textController,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Please enter rate of interest';
                      }
                    },
                    decoration: InputDecoration(
                        labelText: 'Rate of Interest',
                        hintText: 'In percent',
                        labelStyle: textStyle,
                        errorStyle: TextStyle(
                            color: Colors.yellowAccent, fontSize: 15.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                  ),
                ),
                //TermTextFeild
                Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            style: textStyle,
                            controller: tirm_textController,
                            validator: (String value) {
                              if (value.isEmpty) {
                                return 'Please enter tirm';
                              }
                            },
                            decoration: InputDecoration(
                                labelText: 'Term',
                                hintText: 'Time in years',
                                labelStyle: textStyle,
                                errorStyle: TextStyle(
                                    color: Colors.yellowAccent, fontSize: 15.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0))),
                          ),
                        ),
                        Container(
                          width: _minimumPadding * 5,
                        ),
                        Expanded(
                          child: DropdownButton<String>(
                            items: _currencies.map((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            onChanged: (String newValaueSelected) {
                              _onDropDownItemSelected(newValaueSelected);
                            },
                            value: _currenciesSelected,
                          ),
                        )
                      ],
                    )),
                //submittButtonTextFeild
                Padding(
                  padding: EdgeInsets.only(
                      top: _minimumPadding, bottom: _minimumPadding),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          color: Theme.of(context).accentColor,
                          textColor: Theme.of(context).primaryColorDark,
                          child: Text(
                            'Calculator',
                            textScaleFactor: 1.3,
                          ),
                          onPressed: () {
                            setState(() {
                              if (_FromKey.currentState.validate()) {
                                this.displayResult = _calculateResult();
                              }
                            });
                          },
                        ),
                      ),
                      Container(width: 10.0),
                      Expanded(
                        child: RaisedButton(
                          color: Theme.of(context).primaryColorDark,
                          textColor: Theme.of(context).primaryColorLight,
                          child: Text(
                            'Rest',
                            textScaleFactor: 1.3,
                          ),
                          onPressed: () {
                            setState(() {
                              _allReset();
                            });
                          },
                        ),
                      )
                    ],
                  ),
                ),
                //ResultTextFeild
                Padding(
                  padding: EdgeInsets.only(
                      top: _minimumPadding, bottom: _minimumPadding),
                  child: Text(
                    this.displayResult,
                    style: textStyle,
                  ),
                ),

                Padding(
                    padding: EdgeInsets.all(_minimumPadding),
                    child:  GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => SecondScreen()),
                        );
                      }, // handle your image tap here
                      child: Image.asset(
                        'images/right_arrow.png',
                        //fit: BoxFit.cover, // this is the solution for border
                        width: 50.0,
                        height: 50.0,
                        color: Colors.blue,
                      ),

                    )

                ),
              ],
            ),
          ),
        ));
  }

  Widget getImageAsset() {
    AssetImage assetImage = AssetImage('images/right_arrow.png');
    Image image = Image(
      image: assetImage,
      width: 120.0,
      height: 120.0,
      color: Colors.blue,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(
          left: _minimumPadding * 20,
          top: _minimumPadding,
          bottom: _minimumPadding * 10),
    );
  }

  Widget getImageAsset2() {
    Material(
        child: InkWell(
      onTap: () {},
      child: Container(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Image.asset('images/right_arrow.png',
              width: 110.0, height: 110.0),
        ),
      ),
    ));
    /*GestureDetector(
      onTap: (){},
      child: Image.asset('images/right_arrow.png',
        fit: BoxFit.cover, // this is the solution for border
        width: 110.0,
        height: 110.0,
      ),
    );*/
    /*AssetImage assetImage = AssetImage('images/right_arrow.png');
    Image image = Image(
      image: assetImage,
      width: 50.0,
      height: 50.0,
      color: Colors.blue,
    );
    return Container(
      child: image,
      margin: EdgeInsets.only(left: _minimumPadding*5,top: _minimumPadding*5, bottom: _minimumPadding*20),
    );*/
  }

  void _onDropDownItemSelected(String newValaueSelected) {
    setState(() {
      _currenciesSelected = newValaueSelected;
    });
  }

  String _calculateResult() {
    double principle = double.parse(principal_textController.text);
    double interest = double.parse(interest_textController.text);
    double tirm = double.parse(tirm_textController.text);
    double totalAmountPayble = principle + (principle * interest * tirm) / 100;
    String result =
        'After $tirm years, your investment will be worth $totalAmountPayble $_currenciesSelected';
    return result;
  }

  void _allReset() {
    principal_textController.text = '';
    interest_textController.text = '';
    tirm_textController.text = '';
    displayResult = '';
    _currenciesSelected = _currencies[0];
  }
}
